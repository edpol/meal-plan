<!DOCTYPE html>
<html>
<head>
<title><?= $title ?></title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script type='text/javascript' src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type='text/javascript' src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?= HOME; ?>kickstyles.css" />
<script type="text/javascript" src="<?= HOME; ?>mysrc.js"></script>
</head>
<body>
<div id="wrapper">
	<form id="form1" method="post" action="<?= HOME; ?>index.php">
	<header>
		<div style="background-color:black; border-radius:24px; ">
			<div id="logo">
				<img src="<?= HOME; ?>images/header.gif" width=50% alt="KickStart45" />
			</div>
			<div id="instructions">
				To add an item to a meal<br />
				<ol>
					<li>Select an item from the left column.</li>
					<li>Select the meals you want to add the item to.</li>
					<li>Then press the meal button.</li>
				</ol>
			</div>
			<br clear="all" />
		</div>
		<div class="buttons">
<?php		$blue = 'class="blue_up" onMouseUp="this.className=' . "'blue_up'" . '" onMouseDown="this.className=' . "'blue_down'" . '"'; ?>
			<input type="submit" name="btnReset"   value="Reset"     <?= $blue; ?> />
			<input type="submit" name="btnAddMeal" value="Add Meal"  <?= $blue; ?> />
			<a href="<?= HOME; ?>report.php" target="_blank" <?= $blue; ?>>Print</a>
			<input type="submit" name="btnSave"    value="Save"      <?= $blue; ?> />
<?php
			if($show_button) {
				echo "<input type='submit' name='btnGet'    value='Saved Meal' " . $blue . "/>";
			}
?>
		</div>
	</header>
	<div id="wrapper2">
