create database mealplan;
use mealplan;

GRANT ALL PRIVILEGES ON mealplan.* TO 'nut'@'localhost' IDENTIFIED BY 'vitamins';
FLUSH PRIVILEGES;

drop table if exists items;
create table items(
itemid     integer unsigned    not null    auto_increment,
item       varchar(30),
serving    varchar(30),
calories   dec(10,2),
protein    dec(10,2),
carbs      dec(10,2),
fat        dec(10,2),
sodium     dec(10,2),
category   char(1),
primary key(itemid)
);

INSERT INTO items (itemid, item, serving) VALUES (0, "item", "serving");

INSERT INTO items 
(item,                          serving,       calories, protein, carbs,  fat,    sodium, category)
VALUES
("Almond Butter",              "2 tbsp","170","7","5","15","2","F"),
("Almond Milk",                "1 cup","40","0","0","0","168","O"),
("Almonds",                    "24 nuts/ 1oz","160","6","6","14","0","F"),
("Apple, green, granny smith", "1","95","0.5","25","0","2","C"),
("Asparagus",                  "7oz","40","4","8","0.2","4","C"),
("Avocado",                    "1/2","160","9","2","15","7","F"),
("Banana",                     "","105","1","27","0.5","0","C"),
("Berries",                    "1/2 cup","10","0","2","0","0","C"),
("Black Beans, cooked",        "1 cup","240","15","45","1","6","C"),
("Brocolli",                   "2 cups","100","4","16","0","80","C"),
("Brown/Wild Rice, cooked",    "1 cup","140","3","29","1","5","C"),
("Canola Oil",                 "2 tbsp","250","0","0","28","0","F"),
("Cashews",                    "1 oz","160","5","9","12","3","F"),
("Chicken",                    "6oz","280","53","0","5","125","P"),
("Chickpeas, cooked",          "1 cup","270","15","45","4","11","C"),
("Corn",                       "1.5 cup","200","6","42","2","0","C"),
("Egg Whites",                 "1 cup","125","26","2","0","400","P"),
("Eggs",                       "1","80","7","1","5","70","P"),
("Ezekial Bread",              "1 slice","80","4","14","0.5","80","C"),
("Ezekial Cereal",             "1/2 cup","200","8","38","3","190","C"),
("Fage Greek Yogurt",          "plain 1 cup","130","23","9","0","85","P"),
("Farro, cooked",              "1/2 cup","200","7","37","1.5","0","C"),
("Flaxseed oil",               "2 tbsp","240","0","0","28","0","F"),
("Green Beans",                "1 cup","60","2","10","0","0","C"),
("Ground Turkey",              "6oz","190","40","0","3","90","P"),
("Honey",                      "1 tbsp","64","0","17","0","1","O"),
("Kale",                       "1 cup","25","2","3","0","10","C"),
("Ketchup",                    "2 tbspn","40","0","10","0","320","O"),
("Lean Beef",                  "6oz","300","36","0","18","120","P"),
("Lentils, cooked",            "1 cup cooked","230","18","40","1","4","C"),
("Milk",                       "1/2 cup","61","4","6","2.5","50","O"),
("Mustard",                    "1 tbsp","5","0","0","0","55","O"),
("Natural Fit 100",            "1 scoop","140","20","8","4","290","P"),
("Oatmeal, cooked",            "1/2 cup","150","5","27","2.5","0","C"),
("Olive Oil",                  "2 tbsp","240","0","0","28","0","F"),
("PB No Sodium",               "2 Tbsp","180","9","6","15","0","F"),
("PB",                         "2 Tbsp","200","9","6","16","120","F"),
("Peanuts",                    "1 oz","160","7","5","14","5","F"),
("Peppers",                    "1 medium","24","1","6","0","4","C"),
("Pistachios",                 "1 oz","160","6","8","13","0","F"),
("Quinoa, cooked",             "1 cup","223","8.1","40","3.5","13","C"),
("Rice Cakes, Brown No Salt",  "1 cake","60","1","14","0","0","C"),
("Rice Cakes, Brown",          "1 cake","60","1","14","0.5","35","C"),
("Salmon",                     "6oz","350","34","0","22","100","P"),
("Salt",                       "1 teaspoon","0","0","0","0","200","O"),
("Shrimp Cooked",              "6oz","180","36","0","0","360","P"),
("Snapper Cooked",             "6oz","170","34","0","2","110","P"),
("Soy sauce, low sodium",      "1 tbsp","20","1","1","0","575","O"),
("Spinach",                    "2 cup",         "14","2","2","0","48","C"),
("Sunflower Oil",              "2 tbsp","240","0","0","28","0","F"),
("Super Greens",               "3 cups","20","2","3","0","95","C"),
("Sweet Potato",               "medium size","120","2","24","0","41","C"),
("Tuna Cooked",                "6oz","300","45","0","10","90","P"),
("Tuna",                       "4oz","120","26","0","0","280","P"),
("Watermelon",                 "1 cup diced","46","1","11","0","2","C"),
("Whey protein, typical brand","1 scoop","175","28","2","1","100","P"),
("White Potato",               "medium size","165","4","37","0","13","C"),
("Yams",                       "medium size","158","2","37","0","11","C");

