<?php 
	include("../includes/initialize.php");

	$array = array("dir"=>__DIR__, "title"=>"Meal Plan");
	render("report",$array);

	$meals = $_SESSION["meals"];
	if (empty($meals)) { 
		echo "no meals"; 
	} else {
		try {
			$db = open_db();
			echo list_meals($meals,true);
		}
		catch(PDOException $e) {
	    	echo $e->getMessage();
	    }
	}
	echo '<br clear="all" />';

	include (VIEWS.DS."footer.php"); 
?>