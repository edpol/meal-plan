<?php 
	include("../includes/initialize.php");
	
	$print = false;
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    	//something posted

		// the print button calls a different page, never get here
	    if (isset($_POST['btnPrint'])) {
//			echo "Pressed Print Button <br />";
			$print = true;
		}
	}

	// Initialize $meals
    if (isset($_POST['btnGet'])) {
		$cookie_name = "mealplan";
		if(isset($_COOKIE[$cookie_name])) {
			$meals = convert_cookie($_COOKIE[$cookie_name]);
		}
	} else {
		$meals = initialize_meals();
	}

	// Reset button, delete session variable or initialize_meals will copy it into $meals
	if (isset($_POST['btnReset'])) {
		$meals = reset_meals();
		unset($_POST['btnReset']);
	}

	//	Pressed Update Button
	if (isset($_POST['btnUpdate'])) {
		$meals = update_meals($_POST);
		unset($_POST['btnUpdate']);
		$_SESSION["meals"] = $meals;
		redirect_to("index.php");
	}

	// Deleting Item
	if (isset($_POST["delete"])) {
		$i = $_POST["delete"];
		$count = count($meals);
		$k = $count - 1;
		for ($j=$i; $j<$k; $j++) {
			$meals[$j] = $meals[$j+1];
		}
		unset($meals[$k]);
		unset ($_POST["delete"]);
		// If they refresh the screen it's going to keep deleting unless we redirect.
		$_SESSION["meals"] = $meals;
		redirect_to("index.php");
	}

	// bubble sort on $meals
	$meals = bubble($meals);

	// Add meal AFTER sorted, will bubble erase an empty meal?
    if (isset($_POST['btnAddMeal'])) {
		$i = count($meals) - 1;
		$next_meal = $meals[$i]["meal"]+1;
		$meals[] = array( "meal"=>$next_meal, "itemid"=>"0", "qty"=>"0" );
	}

	// save $meals to cookie
	$cookie_name = "mealplan";
    if (isset($_POST['btnSave'])) {
		$cookie_value = "";
		foreach ($meals as $key => $value) $cookie_value .= implode(",",$value) . ";";
		setcookie ($cookie_name, $cookie_value, time()+(60*60*24*365)); //, "/kickstart45/", "kickstart45.com", 1);
//we should use javascript to add the button
		$show_button = true;
	} else {
		$show_button = (isset($_COOKIE[$cookie_name])) ? true : false;
	}

	$_SESSION["meals"] = $meals;

	$array = array("dir"=>__DIR__, "title"=>"Meal Plan", "show_button"=>$show_button );
	render("header",$array);

//		echo $cookie_value . "<br />";

	try {
		$db = open_db();
		echo list_items();

		echo list_meals($meals);

	}
	catch(PDOException $e) {
	    echo $e->getMessage();
    }

	echo "<br clear='all' />\n";

	include (VIEWS.DS."footer.php"); 
?>