
	function pressed(e) {
		// Has the enter key been pressed?
//		if ( (window.event ? event.keyCode : e.which) == 13) { 
		if ( (window.event ? e.keyCode : e.which) == 13) { 
			// If it has been so, manually submit the <form>
			document.forms[0].submit();
		}
	}

	// Submit when you click on element
	function submitForm(action) {
		document.getElementById('form1').action = action;
		document.getElementById('form1').submit();
	}

	// Submit form when CR is pressed, ASCII code #13
	function submitEnter(action,e) { 
		if ( (window.event ? e.keyCode : e.which) == 13) { 
			document.getElementById('form1').action = action;
			document.getElementById('form1').submit();
		}
	}

	function change_focus(e) {
		if ( (window.event ? e.keyCode : e.which) == 13) { 
			document.getElementById('nextitem').focus(); 
			document.getElementById('orderno').blur(); 
		}
	}

	function setFocusToTextBox(){
	    document.getElementById("itemno").focus();
	}

	function submitForm(action) {
		document.getElementById('form1').action = action;
		document.getElementById('form1').submit();
	}

	function myFunction(event) {
    	var x = event.which || event.keyCode;
    	document.getElementById("demo").innerHTML = "The Unicode value is: " + x;
	}

	$(function() {
		$( "#datepicker, #datepicker2" ).datepicker();
	});


   /*
	*	Fade div's
	*/
	setTimeout(function() {
	    $('#showup').delay(5000).slideUp( 2000 );
	}, 1000); // <-- time in milliseconds

	setTimeout(function() {
	    $('#showupinq').delay(50000).slideUp( 2000 );
	}, 1000); // <-- time in milliseconds

   /*
	*	Show and hide any div
	*/
	function showHide(id) {
		var el = document.getElementById(id);
		if( el && el.style.display == 'block')    
			el.style.display = 'none';
		else 
			el.style.display = 'block';
	}
	function showHide2(id,newdisplay) {
		var el = document.getElementById(id);
		if( el && el.style.display == 'block')    
			el.style.display = 'block';
		else 
			el.style.display = 'none';
	}
	function showid(id) {
		var el = document.getElementById(id);
		el.style.display = 'block';
		alert("show");
	}
	function hideid(id) {
		var el = document.getElementById(id);
		el.style.display = 'none';
		alert("hide");
	}
	function swap(on, off) {
		document.getElementById(on).style.display = 'block';
		document.getElementById(off).style.display = 'none';
	}


	/*
	 *	Drag Drop and Clone
	 */
//<![CDATA[
window.onload=function(){
var x = null;
//Make element draggable
$(".drag").draggable({
    helper: 'clone',
    cursor: 'move',
    tolerance: 'fit',
    revert: true
});

$("#droppable").droppable({
    accept: '.drag',
    activeClass: "drop-area",
    drop: function (e, ui) {
        if ($(ui.draggable)[0].id != "") {


            x = ui.helper.clone();
            ui.helper.remove();
            x.draggable({
                helper: 'original',
                cursor: 'move',
                //containment: '#droppable',
                tolerance: 'fit',
                drop: function (event, ui) {
                    $(ui.draggable).remove();
                }
            });

            x.resizable({
                maxHeight: $('#droppable').height(),
                maxWidth: $('#droppable').width()
            });
            x.addClass('remove');
            var el = $("<span><a href='Javascript:void(0)' class=\"xicon delete\" title=\"Remove\">X</a></span>");
            $(el).insertAfter($(x.find('img')));
            x.appendTo('#droppable');
            $('.delete').on('click', function () {
                $(this).parent().parent('span').remove();
            });
            $('.delete').parent().parent('span').dblclick(function () {
                $(this).remove();
            });
        }
    }
});

$("#remove-drag").droppable({
    drop: function (event, ui) {
        $(ui.draggable).remove();
    },
    hoverClass: "remove-drag-hover",
    accept: '.remove'
});
}//]]> 
