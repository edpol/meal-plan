<?php 

	function redirect_to( $location = NULL ) {
		if ($location != NULL) {
			header("Location: {$location}");
			exit;
		}
	}

	function open_db () {
		require_once(LIB_PATH.DS.'connect.php');
		$attributes = array();
		$attributes[PDO::ATTR_EMULATE_PREPARES] = false;
		$attributes[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$attributes[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
		$attributes[PDO::ATTR_CURSOR] = PDO::CURSOR_SCROLL;
		$db = new PDO( "mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASSWORD, $attributes );
		return $db;
	}

	function tabs($i=1) {
		if (!is_int($i)) $i=1;
		return str_repeat("\t",$i);
	}

	// Don't show the .0
	function fdecimal ($d) {
		$output = sprintf("%.1f",$d);
		if (substr($output,-2)==".0") {
			$output = substr($output,0,-2);
		}
		return $output;
	}

	function list_items_r() {
		global $db;
		$sql = "select * from items where itemid>0 ";

		$stmt = $db->query($sql);
		$count = $stmt->rowCount();

		$msg = false;
		$first = true;
		$col = array();

		if ($count==0) {
			$msg = "\tCount: $count\n";
		} else {
			$m = array("", "text-align:left;  width:220px;","text-align:left;  width:100px;","text-align:right; width:40px;","text-align:right; width:60px;","text-align:right; width:45px;","text-align:right; width:40px;","text-align:right; width:55px;");
			$h = array("", "item",                          "serving",                       "cal",                          "protein",                      "carbs",                        "fat",                          "sodium");

			for ($i=0; $i<8; $i++) {
				$col[$i]  = tabs(3) . "<div class='drop' style='". $m[$i] . "'>\n" . tabs(4) . "<i><u>" .$h[$i] . "</u></i><br />";
			}

			for ($i=1; $i<=$count; $i++) {
			    $obj = $stmt->fetch(PDO::FETCH_OBJ);
				$col[0] .= "\n" . tabs(4) . "<input type='checkbox' name='itemid" . $obj->itemid . "'/><br />";
				$col[1] .= $obj->item              . "<br />";
				$col[2] .= $obj->serving           . "<br />";
				$col[3] .= fdecimal($obj->calories). "<br />";
				$col[4] .= fdecimal($obj->protein) . "<br />"; 
				$col[5] .= fdecimal($obj->carbs)   . "<br />"; 
				$col[6] .= fdecimal($obj->fat)     . "<br />"; 
				$col[7] .= fdecimal($obj->sodium)  . "<br />";
			}

			$msg = tabs(2) . "<div class='whitebox'>\n";
			for ($i=0; $i<8; $i++) {
				$msg .= $col[$i] . "\n" . tabs(3) . "</div>\n";
			}
			$msg .= tabs(3) . "<br clear='all' />\n";
			$msg .= tabs(2) . "</div>\n";
		}
		return $msg;
	}

	function build_category_list() {
		$category_list[0] = array("category"=>"P", "title"=>"Protein");
		$category_list[1] = array("category"=>"F", "title"=>"Fat");
		$category_list[2] = array("category"=>"C", "title"=>"Carbohydrate");
		$category_list[3] = array("category"=>"O", "title"=>"Other");
		return $category_list;
	}

	function build_column_info() {
		$column_info    = array();
		$column_info[0] = array("style"=>"",                               "heading"=>""); // checkbox
		$column_info[1] = array("style"=>"text-align:left;  width:218px;", "heading"=>"item");
		$column_info[2] = array("style"=>"text-align:left;  width:100px;", "heading"=>"serving");
		$column_info[3] = array("style"=>"text-align:right; width:40px;",  "heading"=>"cal");
		$column_info[4] = array("style"=>"text-align:right; width:58px;",  "heading"=>"protein");
		$column_info[5] = array("style"=>"text-align:right; width:45px;",  "heading"=>"carbs");
		$column_info[6] = array("style"=>"text-align:right; width:50px;",  "heading"=>"fat");
		$column_info[7] = array("style"=>"text-align:right; width:55px;",  "heading"=>"sodium");
		$column_info[8] = array("style"=>"text-align:right; width:20px;",  "heading"=>""); // delete row
		return $column_info;
	}

	function list_items() {
		global $db;
		$category_list = build_category_list();
		$column_info   = build_column_info();

		$msg = tabs(2) . "<div class='whitebox'>\n";

		foreach($category_list as $key => $value) {
			extract ($value);
			$msg .= tabs(3) . "<div align='center' class='category_heading color_{$category}'>{$title}</div>\n";
			for ($i=0; $i<8; $i++) { $col[$i] = ""; }

			// Open div and headings for columns 0-7
			for ($i=0; $i<8; $i++) {
				extract($column_info[$i]);
				$col[$i] .= tabs(3) . "<div class='drop color_{$category}' style='{$style}'>\n";
				$col[$i] .= tabs(4) . "<i><u>{$heading}</u></i><br />";
			}

			$sql = "select * from items where category='{$category}' ";
			$stmt = $db->query($sql);
			$count = $stmt->rowCount();

			if ($count>0) {
				column_list($stmt,$count,$col);
			}
			for ($i=0; $i<8; $i++) {
				$col[$i] .= "\n" . tabs(3) . "</div>\n";
				$msg .= $col[$i];
			}
			$msg .= tabs(3) . "<br clear='all' />\n";
		}
		$msg .= tabs(2) . "</div>\n <!-- close whitebox -->";

		return $msg;
	}

	function column_list($stmt,$count,&$col) {
		global $db;
//		$col = array();
		for ($i=1; $i<=$count; $i++) {
			$obj = $stmt->fetch(PDO::FETCH_OBJ);
			$col[0] .= "\n" . tabs(4) . "<input type='checkbox' name='itemid" . $obj->itemid . "'/><br />";
			$col[1] .= $obj->item              . "<br />";
			$col[2] .= $obj->serving           . "<br />";
			$col[3] .= fdecimal($obj->calories). "<br />";
			$col[4] .= fdecimal($obj->protein) . "<br />"; 
			$col[5] .= fdecimal($obj->carbs)   . "<br />"; 
			$col[6] .= fdecimal($obj->fat)     . "<br />"; 
			$col[7] .= fdecimal($obj->sodium)  . "<br />";
		}
//		return $col;
	}

	function render ($template, $data=array("dir"=>PUBLIC_ROOT, "title" => "Rejuvenetics")) {
		$path = VIEWS . DS . $template . '.php';
		extract($data);
		$c = substr_count($dir, DS) - substr_count(PUBLIC_ROOT, DS);
		$levels = str_repeat("../", $c);
		if (file_exists($path)) {
			require($path);
		}
	}

	function print_array($target) {
		echo "<pre>";
		print_r($target);
		echo "</pre>";
	}

	// for this to work you must have blue_up and blue_down defined in css
	function myButton ($color="blue",$target="",$value="Submit") {
		$msg  = '<input	type="button" ';
		$msg .= 'onClick="submitForm(' . "'" . $target . "'" . ')" ';
		$msg .= 'value="' . $value . '" '; 
		$msg .= 'class="' . $color . '_up" ';  
		$msg .= 'onMouseUp="this.className='   . "'" . $color . "_up'"   . '" ' ;
		$msg .= 'onMouseDown="this.className=' . "'" . $color . "_down'" . '" />';
		return $msg;
	}


	function grand_total($sum, &$grand_total, &$gt) {
		if (gettype($sum) == "array") {
			$grand_total[] = $sum;
			foreach ($sum as $key => $value) {
				if (!isset($gt[$key])) $gt[$key] = 0;
				$gt[$key] += $sum[$key];
			}
		}
	}

	function append($col, $sum, &$grand_total, &$gt) {
		grand_total($sum, $grand_total, $gt);
		$one_meal = "";
		if (gettype($col) == "array") {
			for ($i=0; $i<8; $i++) {
				$one_meal .= $col[$i] . "\n"; 
				if ($i>=2 && $i<7) {
					$one_meal .= tabs(5) . "<hr />\n";
					$one_meal .= tabs(5) . $sum[$i] . "\n";
				}
				$one_meal .= tabs(4) . "</div>\n";
			}
			$one_meal .= tabs(4) . "<br clear='all' />\n";
		}
		return $one_meal;
	}

	// return whaever value $close_div has
	// if $close_div is empty change $close_div to not be empty NEXT time around
	function close_div () {
		global $close_div;
		$msg = "";
		if (empty($close_div)) {
			$close_div  = tabs(3) . "</div> <!-- Close Grey -->\n";
			$close_div .= tabs(3) . "<br /> <!-- x -->\n";
			$close_div .= tabs(3) . "\n";
		} else {
			$msg = $close_div;
		}
		return $msg;
	}

	function list_meals ($meals,$print=false) {

		$list = "\n\t\t<div id='list_meals'>\n";
		$count = count($meals);

		$sblue = 'class="sblue_up" onMouseUp="this.className='."'sblue_up'". '" onMouseDown="this.className=' . "'sblue_down'". '"'; 
		$col = ""; $sum = "";
		$meal_index = -1;
		$close_div = "";
		$gt = array();
		$grand_total = array();
//		$m = array("text-align:left;  width:220px;","text-align:left;  width:100px;","text-align:right; width:40px;","text-align:right; width:60px;","text-align:right; width:45px;","text-align:right; width:40px;","text-align:right; width:55px;","text-align:right; width:20px;");
//		$h = array("item",                          "serving",                       "cal",                          "protein",                      "carbs",                        "fat",                          "sodium",                       "");
		$column_info = build_column_info();

		for ($idx = 0; $idx<$count; $idx++) {
			$line = $meals[$idx];

			// Close previous div and open new one
			if ($meal_index<>$line["meal"]) {

				// Close Div
				$meal_index =$line["meal"];
				$list .= append($col,$sum,$grand_total, $gt);
				$list .= close_div();

				// Open Div
				$list .= tabs(3) . "<div class='greybox'>\n";
				if (isset($print) && $print) 
					$list .= "Meal #{$meal_index}<br />\n";
				else
					$list .= tabs(4) . "<input type='submit' name='btnUpdate' value='Meal{$meal_index}' {$sblue} /><br />\n";

				for ($i=0; $i<8; $i++) {
					extract($column_info[$i+1]);
					$col[$i]  = tabs(4) . "<div class='drop' style='{$style}'>\n"; 
					$col[$i] .= tabs(5) . "<i><u>{$heading}</u></i><br />";
				}
				for ($i=2; $i<=6; $i++) { $sum[$i] = 0; }
			}

			$obj = get_item($line, $idx, $col, $sum, $print);
		}

		$list .= append($col,$sum,$grand_total, $gt);

		$list .= close_div(); // $close_div;

//grand_total
		$list .= tabs(3) . "<div class='greybox'>\n";
		$list .= tabs(4) . "GRAND TOTAL<br />\n";
		$h = array("", "", "cal", "protein", "carbs", "fat", "sodium", "");
		for ($i=0; $i<8; $i++) {
			extract($column_info[$i+1]);
			$col[$i]  = tabs(4) . "<div class='drop' style='{$style}'>\n"; 
			if ($i<2) 
				$col[$i] .= tabs(5) . "&nbsp;";
			else
				$col[$i] .= tabs(5) . "<i><u>{$heading}</u></i>";
			$col[$i] .= "<br />";
		}
		$list .= append2($col, $gt);
		$list .= close_div();

		$list .= "\t\t</div>\t<!-- end of list_meals -->\n\n";
		return $list;
	}

	function list_meals2 ($meals,$print=false) {

		$list = "\n\t\t<div style='float:left;'>\n";
		$count = count($meals);

		$sblue = 'class="sblue_up" onMouseUp="this.className='."'sblue_up'". '" onMouseDown="this.className=' . "'sblue_down'". '"'; 
		$col = ""; $sum = "";
		$meal_index = -1;
		$close_div = "";
		$gt = array();
		$grand_total = array();
		$m = array("text-align:left;  width:220px;","text-align:left;  width:100px;","text-align:right; width:40px;","text-align:right; width:60px;","text-align:right; width:45px;","text-align:right; width:40px;","text-align:right; width:55px;","text-align:right; width:20px;");
		$h = array("item",                          "serving",                           "cal",                          "protein",                      "carbs",                        "fat",                          "sodium",                       "");

		for ($idx = 0; $idx<$count; $idx++) {
			$line = $meals[$idx];

				// Close previous div and open new one
				if ($meal_index<>$line["meal"]) {

					// Close Div
					$meal_index =$line["meal"];
					$list .= append($col,$sum,$grand_total, $gt);
					$list .= close_div();

					// Open Div
					$list .= tabs(3) . "<div class='greybox'>\n";
					if (isset($print) && $print) 
						$list .= "Meal #{$meal_index}<br />\n";
					else
						$list .= tabs(4) . "<input type='submit' name='btnUpdate' value='Meal{$meal_index}' {$sblue} /><br />\n";

					for ($i=0; $i<8; $i++) {
						$col[$i]  = tabs(4) . "<div class='drop' style='" . $m[$i] . "'>\n"; 
						$col[$i] .= tabs(5) . "<i><u>" .$h[$i] . "</u></i><br />";
					}
					for ($i=2; $i<=6; $i++) { $sum[$i] = 0; }
				}

			$obj = get_item($line, $idx, $col, $sum, $print);
		}

		$list .= append($col,$sum,$grand_total, $gt);

		$list .= close_div(); // $close_div;

//grand_total
		$list .= tabs(3) . "<div class='greybox'>\n";
		$list .= tabs(4) . "GRAND TOTAL<br />\n";
		$h = array("", "", "cal", "protein", "carbs", "fat", "sodium", "");
		for ($i=0; $i<8; $i++) {
			$col[$i]  = tabs(4) . "<div class='drop' style='float:left; " . $m[$i] . "'>\n"; 
			$col[$i] .= tabs(5) . "<i><u>" .$h[$i] . "</u></i><br />";
		}
		$list .= append2($col, $gt);
		$list .= close_div();

		$list .= "\t\t</div>\t<!-- end of list_meals -->\n\n";
		return $list;
	}

	function append2($col, $gt) {
//		grand_total($sum, $grand_total, $gt);
		$one_meal = "";
		if (gettype($col) == "array") {
			for ($i=0; $i<8; $i++) {
				$one_meal .= $col[$i] . "\n"; 
				if ($i>=2 && $i<7) {
//					$one_meal .= tabs(5) . "<hr />\n";
					$one_meal .= tabs(5) . $gt[$i] . "\n";
				}
				$one_meal .= tabs(4) . "</div>\n";
			}
			$one_meal .= tabs(4) . "<br clear='all' />\n";
		}
		return $one_meal;
	}


	function get_item ($line, $idx, &$col, &$sum, $print) {
		global $db;
		extract($line);

		$obj = false;
		if (isset($itemid) && !empty($itemid)) {
			$sql = "select * from items where itemid={$itemid} ";
			$stmt = $db->query($sql);
			$count = $stmt->rowCount();
			$add = array ();
			if ($count==1) {
			    $obj = $stmt->fetch(PDO::FETCH_OBJ);

				$add[2]  = (float) fdecimal($obj->calories)*$qty;
				$add[3]  = (float) fdecimal($obj->protein) *$qty; 
				$add[4]  = (float) fdecimal($obj->carbs)   *$qty; 
				$add[5]  = (float) fdecimal($obj->fat)     *$qty; 
				$add[6]  = (float) fdecimal($obj->sodium)  *$qty;

				$span = "<span class='color_" . $obj->category . "'>";
				$col[0] .= $span . $obj->item                   . "</span><br />";
//				$col[1] .= $span . $obj->serving . " x {$qty} " . "</span><br />";
				$col[1] .= $span . $obj->serving                . "</span><br />";
				if (!$print) {
/*
	<button onClick="submitForm('index.php')" 
	value='0'  type="submit"
	name='delete' 
	class='del_up' 
	onMouseUp="this.className='del_up'" 
	onMouseDown="this.className='del_down'">X</button> 
*/			

					$col[7] .= "\n" . tabs(5) . '<button type="submit" ';
					$col[7] .= "id='del{$idx}' ";
					$col[7] .= "value='{$idx}' name='delete' class='del_up' ";
					$col[7] .= 'onMouseUp="this.className=' . "'del_up'" . '" '; 
					$col[7] .= 'onMouseDown="this.className=' . "'del_down'" . '"/>X</button>';


				}
				for ($i=2; $i<=6; $i++) {
					$col[$i] .= $span . $add[$i] . "</span><br />";
					$sum[$i] += $add[$i];
				}
			}
		}
		return $obj;

	}

	function list_my($meals,$label) {
		$msg = $label . "<br />";
		foreach ($meals as $key => $value) {
			extract($value);
//			$msg .= sprintf("%02d: %02d %02d %02d", $key, $meal, $itemid, $qty) . "<br />";
			$msg .= "{$key}: {$meal} {$itemid} {$qty} <br />";
		}
		return $msg."<br />";
	}

	function bubble ($meals) {
		$count = count($meals);

		// Delete row if qty = 0
		$check_qty_0 = $meals;
		$meals = array();
		foreach($check_qty_0 as $key => $value) {
			if ($value["qty"]>0) {
				$meals[] = $value;
			}
		}
		$count = count($meals);

		//	Need to sort by meal then by itemid	
		$moved = true;
		while ($moved) {
			$moved = false;
			for ($i = 1; $i<$count; $i++) {
				$line1 = $meals[$i-1];
				$line2 = $meals[$i];
				// if the meal in line1 is larger than the meal in line2 switch
				// if they are the same and the itemid1 is larger than itemid2 switch
				if ( ($line1["meal"]>$line2["meal"]) ||
				     ($line1["meal"]==$line2["meal"] && $line1["itemid"]>$line2["itemid"]) ) {
					$meals[$i-1] = $line2;
					$meals[$i]   = $line1;
					$moved = true;
				}
			}
		}

		// build new index values
		// ie - meal1, meal2, meal10, meal20 => meal1, meal2, meal3, meal4
		$i = 1;
		$old_key = -1234;
		$new_i[] = ""; // set index=0
		foreach ($meals as $key => $value) {
			if ($old_key!=$value["meal"]) {
				$old_key =$value["meal"];
				$new[$old_key] = $i++;
				$new_i[] = $old_key;
			}
		}

		// apply new meal index
		for ($i = 0; $i<$count; $i++) {
			$j = $meals[$i]["meal"];
			$meals[$i]["meal"] = $new[$j];
		}

		// meal cant be empty
		if (empty($meals)) $meals = array( array( "meal" => "1",  "itemid" =>  "0", "qty" => "0" ) );

		return $meals;
	}

	function update_meals($post) {
		$meals = $_SESSION["meals"];
		$target = substr($post["btnUpdate"]." ",4, -1);
		$meal_list = array($target);
		$item_list = array();
		// create list of meals and items selected
		foreach ($post as $key => $value) {
			if (substr($key,0,6) == "itemid") {
				$item_list[] = substr($key,6,strlen($key)) ;
			}
		}
		// now we add the selected items to the selected meals
		foreach ($meal_list as $keym => $valuem) {
			foreach ($item_list as $keyi => $valuei) {
				$meals[] = array ("meal"=>"{$valuem}" , "itemid"=>"{$valuei}" , "qty"=>"1");
				//echo $keym . " " . $valuem . " " . $keyi . " " . $valuei . "<br />";
			}
		}
		$_SESSION["meals"] = $meals;
		return $meals;
	}

	function dump_meals ($meals,$color="green") {
		$table  = "<span>\n";
		$table .= "<table style='color:{$color};' border=1>\n";
		$table .= "<tr><th>key</th><th>meal</th><th>ID</th><th>qty</th></tr>\n";
		foreach ($meals as $key => $value) {
			// explode string to array
			// implode array to string
			// extract array to variables
			extract($value);
			$table .= sprintf("<tr><td align='right'>%d</td><td align='right'>%d</td><td align='right'>%3d</td><td align='right'>%3d</td></tr>", $key, $meal, $itemid, $qty) ;
		}
		unset($key);
		unset($meal);
		unset($itemid);
		unset($qty);
		$table .= "</table>\n";
		$table .= "</span>\n";
		return $table;
	}

	function reset_meals() {
		unset($_SESSION["meals"]);
		$meals = array( array( "meal" => "1",  "itemid" =>  "0", "qty" => "0" ) );
//		$meals = initialize_for_debug ($meals);
		return $meals;
	}

	function initialize_meals() {
		if (!isset ($_SESSION["meals"])) {
			$meals = reset_meals();
		} else {
			$meals = $_SESSION["meals"];
		}
		return $meals;
	}

	function initialize_for_debug ($meals) {
		$meals = array( array( "meal" =>  "7", "itemid" => "10", "qty" => "2" ), 
						array( "meal" => "11", "itemid" => "15", "qty" => "3" ), 
						array( "meal" =>  "7", "itemid" => "15", "qty" => "2" ), 
						array( "meal" =>  "2", "itemid" => "11", "qty" => "8" ), 
						array( "meal" => "11", "itemid" => "10", "qty" => "4" ), 
						array( "meal" =>  "7", "itemid" =>  "5", "qty" => "3" ), 
						array( "meal" =>  "7", "itemid" => "18", "qty" => "1" ), 
						array( "meal" =>  "4", "itemid" => "10", "qty" => "3" ), 
						array( "meal" => "11", "itemid" => "12", "qty" => "1" ) );	
		return $meals;
	}

	// Items are seperated by ;
	// Item details are seperated by ,
	function convert_cookie ($cookie) {
		$output = array ();
		$list = explode(";",$cookie);

		foreach ($list as $key => $value) {
			if (!empty($value)) { 
				$one_item=explode(",",$value);
				$output[] = array( "meal" => $one_item[0],  "itemid" => $one_item[1], "qty" => $one_item[2] );
			}
		}
		return $output;
	}

	function isSecure() {
		return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
	}

	/*
	 *	returns url of present default
	 *	The SSL certificate is for "www.*", so add it if it's missing
	 */
	function default_location () {
		$uri = $_SERVER['REQUEST_URI'];
		$pos = strrpos($_SERVER['REQUEST_URI'], "/");
		if ($pos === false) { // note: three equal signs
			$folder = "";
		} else {
			$folder = substr($uri,0,$pos);
		}
		$server_host = $_SERVER['HTTP_HOST'];
		if ($_SERVER['HTTP_HOST']=='localhost') {
			$http = "";
			$www  = "";
			$server_host="";
		} else {
			$www = (substr($_SERVER['HTTP_HOST'],0,4)=="www.") ? '' : "www.";
			$http = (isSecure()) ? "https://" : "http://";
		}
		$default = "{$http}{$www}{$server_host}{$folder}/";
		return $default;
	}

?>