<?php

//db connection class using singleton pattern
class dbConn{

	//variable to hold connection object.
	protected static $db;

	//private construct - class cannot be instatiated externally.
	private function __construct() {

		try {
			// assign PDO object to db variable
			self::$db = new PDO( 'mysql:host=localhost;dbname=mealplan','nut','vitamins' );
			self::$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION,PDO::FETCH_ASSOC);
		}
		catch (PDOException $e) {
			//Output error - would normally log this to error file rather than output to user.
			echo "Connection Error: " . $e->getMessage();
		}
	}


	// get connection function. Static method - accessible without instantiation
	public static function getConnection() {

		//Guarantees single instance, if no connection object exists then create one.
		if (!self::$db) {
			//new connection object.
			new dbConn();
		}

		//return connection.
		return self::$db;
	}

}//end class

//require_once 'conf.php';


class Didit {
    
    public $conn;
            
    /**
    * Short description for file   
    * @param string  $first_name    description
    * 
    * @return boolean
    */
    
    public function register_email($reg_email)
    {    
        $conn = dbConn::getConnection();
        
        $querychk = $conn->query("select * from User where Email = '$reg_email' ")->rowCount();        
        if($querychk <= 0)
        {            
            $query1 = $conn->prepare("INSERT into User(Email) VALUES (?)");
            $query1exec = $query1->execute(array($reg_email));
            return $query1exec;            
        } else {
            return -1;        
        }        
    }
    
}
    
?>