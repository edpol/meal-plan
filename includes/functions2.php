<?php
	function list_items2() {
		global $db;
		$msg = false;
		$sql = "select * from items ";

	    $stmt = $db->query($sql);
		$count = $stmt->rowCount();
		$first = true;

		if ($count==0) {
			$msg = "\tCount: $count\n";
		} else {

			$m = array("width:220px","width:100px","width:40px","width:60px","width:40px","width:40px","width:55px");

			$header  = tabs(2);
			$header .= "<span style='margin:0 5px 5px 0; " . $m[0] . ";'>item</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[1] . ";'>serving</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[2] . ";'>cal</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[3] . ";'>protein</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[4] . ";'>carbs</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[5] . ";'>fat</span>";
			$header .= "<span style='margin:0 5px 5px 0; " . $m[6] . ";'>sodium</span>";

			$top  = tabs(2) . "<div class='col bluebox'  id='droppable'></div>\n";
			$top .= tabs(2) . "<div class='col whitebox' id='col1'>\n";
			$top .= $header;
			$top .= tabs(3) . "<div id='drag-list' class='drag-list'>\n";
			$top .= tabs(4) . "<ul>\n";

			$bot  = tabs(4) . "</ul>\n";
			$bot .= tabs(3) . "</div>\n";
			$bot .= tabs(2) . "</div>\n";

			$msg  = "";
			$drag = 1;

			for ($i=1; $i<=$count; $i++) {
			    $obj = $stmt->fetch(PDO::FETCH_OBJ);

				$msg .= tabs(5) . "<li>\n";
				$msg .= tabs(6) . "<span id='drag" . $drag++ . "' class='drag'>\n";

				$msg .= tabs(7) . "<span style='" . $m[0] . ";'>" . $obj->item      . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[1] . ";'>" . $obj->serving       . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[2] . ";'>" . $obj->calories  . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[3] . ";'>" . $obj->protein   . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[4] . ";'>" . $obj->carbs     . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[5] . ";'>" . $obj->fat       . "</span>\n";
				$msg .= tabs(7) . "<span style='" . $m[6] . ";'>" . $obj->sodium    . "</span>\n";

				$msg .= tabs(6) . "</span>\n";
				$msg .= tabs(5) . "</li>\n";
			}

			$drop  = tabs(5) . "<li>\n";
			$drop .= tabs(6) . "<div id='remove-drag' style='width: 100%; height: 80px; background-color: #FF8B8B;'>\n";
			$drop .= tabs(7) . "<div style='color:#fff; font-size:20px; text-align:center; padding-top:20px;'>\n";
			$drop .= tabs(8) . "Remove\n";
			$drop .= tabs(8) . "<div class='tip'>Drop here</div>\n";
			$drop .= tabs(7) . "</div>\n";
			$drop .= tabs(6) . "</div>\n";
			$drop .= tabs(5) . "</li>\n";
		}
		return $top . $msg . $drop . $bot;
	}

	function list_items3() {
		global $db;
		$msg = false;
		$sql = "select * from items ";

	    $stmt = $db->query($sql);
		$count = $stmt->rowCount();
		$first = true;

		if ($count==0) {
			$msg = "\tCount: $count\n";
		} else {

			$top  = tabs(1) . "<div class='col' id='droppable'></div>\n";
			$top .= tabs(1) . "<div class='col' id='col1'>\n";
			$top .= tabs(2) . "<div id='drag-list' class='drag-list'>\n";
			$top .= tabs(3) . "<ul>\n";

			$bot  = tabs(3) . "</ul>\n";
			$bot .= tabs(2) . "</div>\n";
			$bot .= tabs(1) . "</div>\n";

			$msg  = "";
			$drag = 1;
			foreach ($stmt as $row) {
				if ($first) {
					$header = tabs(1) . "<span>";
					foreach ($row as $key => $value) {
						$key = str_replace("_", " ", $key);
						$header .= $key . " ";
					}
					$header .= "</span>\n";
					$first = false;
				}
				$msg .= tabs(4) . "<li>\n";
				$msg .= tabs(5) . "<span id='drag" . $drag++ . "' class='drag'>\n\t\t\t\t\t\t";
				foreach ($row as $key => $value) { 
					$msg .= $value . " ";
				}
				$msg .= "\n" . tabs(5) . "</span>\n";
				$msg .= tabs(4) . "</li>\n";
			}

			$drop  = tabs(4) . "<li>\n";
			$drop .= tabs(5) . "<div id='remove-drag' style='width: 100%; height: 80px; background-color: #FF8B8B;'>\n";
			$drop .= tabs(6) . "<div style='color:#fff; font-size:20px; text-align:center; padding-top:20px;'>\n";
			$drop .= tabs(7) . "Remove\n";
			$drop .= tabs(7) . "<div class='tip'>Drop here</div>\n";
			$drop .= tabs(6) . "</div>\n";
			$drop .= tabs(5) . "</div>\n";
			$drop .= tabs(4) . "</li>\n";
		}
		return $header . $top . $msg . $drop . $bot;
	}

	function dump_stock() {
		global $db;
		$msg = false;
		$sql = "select * from items ";
//		$sql .= "where stock_no='44'";

	    $stmt = $db->query($sql);
		$count = $stmt->rowCount();
		$first = true;

		$msg = "<table border=1 class='bluebox'>\n";
		if ($count==0) {
			$msg .= "\t<tr><td colspan='3'>Count: $count</td></tr>\n";
			$msg .= "\t<tr><td colspan='3'>$sql</td></tr>\n";
		} else {

			$msg .= "<tr><th>item</th><th>serving</th><th>cal</th><th>protein</th><th>carbs</th><th>fat</th><th>sodium</th></tr>";

			for ($i=1; $i<=$count; $i++) {
				$obj = $stmt->fetch(PDO::FETCH_OBJ);
				$msg .= "<tr>\n";
				$msg .= "<td>" . $obj->item      . "</td>\n";
				$msg .= "<td>" . $obj->serving       . "</td>\n";
				$msg .= "<td>" . $obj->calories  . "</td>\n";
				$msg .= "<td>" . $obj->protein   . "</td>\n";
				$msg .= "<td>" . $obj->carbs     . "</td>\n";
				$msg .= "<td>" . $obj->fat       . "</td>\n";
				$msg .= "<td>" . $obj->sodium    . "</td>\n";
				$msg .= "</tr>\n";
			}
		}
		$msg .= "</table>\n";
		return $msg;
	}

	function dump_stock2() {
		global $db;
		$msg = false;
		$sql = "select * from items ";
//		$sql .= "where stock_no='44'";

	    $stmt = $db->query($sql);
		$count = $stmt->rowCount();
		$first = true;

		$msg = "<table border=1>\n";
		if ($count==0) {
			$msg .= "\t<tr><td colspan='3'>Count: $count</td></tr>\n";
			$msg .= "\t<tr><td colspan='3'>$sql</td></tr>\n";
		}
		foreach ($stmt as $row) {
			if ($first) {
				$msg .= "\t<tr>\n\t\t";
				foreach ($row as $key => $value) {
					$key = str_replace("_", " ", $key);
					$msg .= "<th>" . $key . "</th>";
				}
				$msg .= "\n\t</tr>\n";
				$first = false;
			}
			$msg .= "\t<tr>\n\t\t";
			foreach ($row as $key => $value) { 
				$align = (is_numeric($value)) ? "text-align:right;" : "text-align:left;";
				$msg .= "<td style=$align>" . $value . "</td>";
			}
			$msg .= "\n\t</tr>\n";
		}
		$msg .= "</table>\n";
		return $msg;
	}

?>