<?php
defined('DS')            ? null : define('DS', DIRECTORY_SEPARATOR);
defined('LIB_PATH')      ? null : define('LIB_PATH', __DIR__);
defined('SITE_ROOT')     ? null : define('SITE_ROOT',     substr(LIB_PATH,  0, strrpos(LIB_PATH,  DS)));
defined('SERVER_ROOT')   ? null : define('SERVER_ROOT',   substr(SITE_ROOT, 0, strrpos(SITE_ROOT, DS)));
defined('VIEWS')		 ? null : define('VIEWS',         SITE_ROOT . DS . "views");
defined('PUBLIC_ROOT')	 ? null : define('PUBLIC_ROOT',   SERVER_ROOT . DS . "html" . DS . "mealplan");

session_start();
require_once(LIB_PATH.DS.'functions.php');
defined('HOME')     	 ? null : define('HOME', default_location()); 

/*
echo "<pre>";
echo "HOME        " . HOME        . "<br />";
echo "LIB_PATH    " . LIB_PATH    . "<br />";
echo "SITE_ROOT   " . SITE_ROOT   . "<br />";
echo "SERVER_ROOT " . SERVER_ROOT . "<br />";
echo "VIEWS       " . VIEWS       . "<br />";
echo "PUBLIC_ROOT " . PUBLIC_ROOT . "<br />";

print_r($_SERVER);
echo "</pre>\n";
*/

?>