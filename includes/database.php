<?php

require_once(LIB_PATH.DS.'connect.php');

class Database {

	public $db;

	function __construct() {
		$this->open_connection();
	}

	private function open_connection() {
		$attributes = array();
		$attributes[PDO::ATTR_EMULATE_PREPARES] = false;
		$attributes[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$attributes[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
		$attributes[PDO::ATTR_CURSOR] = PDO::CURSOR_SCROLL;
		$this->db = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8", 
						DB_USER, 
						DB_PASSWORD, 
						$attributes
				 	);
	}

	private function close_connection() {
		 $this->db = null;
	}
}

$database = new database();
?>